build: Dockerfile
	sudo podman build -t promsio .

run:
	sudo podman pod create --name mon -p 3000:3000 -p 9090:9090
	sudo podman run -d --restart=always --pod=mon --name=prom promsio
	sudo podman run -d --restart=always --pod=mon --name=graf  -e "GF_INSTALL_PLUGINS=grafana-piechart-panel" grafana/grafana

stats:
	sudo podman pod stats --no-stream
 
clean:
	sudo podman pod rm -f mon

toyaml:
	sudo podman generate kube mon >> mon.yaml

up:
	sudo podman play kube ./mon.yaml

gitsetup:
	git config --global user.name "alexg"
	git config --global user.email "alex.granovsky@storing.io"